//
//  ChatRoomViewController.swift
//  realTime
//
//  Created by 이요한 on 06/08/2019.
//  Copyright © 2019 Yo. All rights reserved.
//
import UIKit

class ChatRoomViewController: UIViewController, UITextFieldDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
   
    var messages: [ChatMessage] = [ChatMessage(fromUserId: "", text: "", timesTamp: 0)]
    
    @IBOutlet var item: UINavigationItem!
    @IBOutlet var chatCollectionView: UICollectionView!
    @IBOutlet var sendButton: UIButton!
    @IBOutlet var chatTextField: UITextField!
    
    var groupKey: String? {
        didSet {
            if let key = groupKey {
                fetchMessages()
                DataService.instance.groupRef.child(key).observeSingleEvent(of: .value, with: { (snapshot) in
                    if let data = snapshot.value as? Dictionary<String, AnyObject> {
                        if let title = data["name"] as? String {
                            self.item.title = title
                        }
                    }
                })
            }
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "chatCell", for: indexPath) as! ChatMessageCell
        let message = messages[indexPath.item]
        cell.textLabel.text = message.text
        setupChatCell(cell: cell, message: message)
        if message.text.count > 0 {
            cell.containerViewWidthAnchor?.constant = measuredFrameHeightForEachMessage(message: message.text).width + 32
        }
        return cell
    }
    
    func setupChatCell(cell: ChatMessageCell, message: ChatMessage) {
        if message.fromUserId == DataService.instance.currentUserUid {
            cell.containerView.backgroundColor = UIColor.magenta
            cell.textLabel.textColor = UIColor.white
            cell.containerViewRightAnchor?.isActive = true
        } else {
            cell.containerView.backgroundColor = UIColor.lightGray
            cell.textLabel.textColor = UIColor.black
            cell.containerViewRightAnchor?.isActive = false
            cell.containerViewLeftAnchor?.isActive = true
        }
    }
    
    private func measuredFrameHeightForEachMessage(message: String) -> CGRect {
        let size = CGSize(width: 200, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string: message).boundingRect(with: size, options: options, attributes: [NSAttributedString.Key.font:UIFont.systemFont(ofSize: 16)], context: nil)
    }
    
    
    func fetchMessages() {
        if let groupId = self.groupKey {
            let groupMessageRef = DataService.instance.groupRef.child(groupId).child("messages")
            groupMessageRef.observe(.childAdded, with: { (snapshot) in
                let messageId = snapshot.key
                let messageRef = DataService.instance.messageRef.child(messageId)
                messageRef.observeSingleEvent(of: .value, with: {(snapshot) in
                    guard let dict = snapshot.value as? Dictionary<String, AnyObject> else {
                        return
                    }
                    let message = ChatMessage(fromUserId: dict["fromUserId"] as! String,
                                              text: dict["text"] as! String,
                                              timesTamp: dict["timesTamp"] as! NSNumber
                    )
                    self.messages.insert(message, at: self.messages.count - 1)
                    self.chatCollectionView.reloadData()
                    if self.messages.count >= 1 {
                        
                    }
                })
            })
        }
    }

    @IBAction func sendButtonTap(_ sender: Any) {
        let ref = DataService.instance.messageRef.childByAutoId()
        guard let fromUserId = DataService.instance.currentUserUid else {
            return
        }
        
        let data: Dictionary<String, AnyObject> = [
            "fromUserId": fromUserId as AnyObject,
            "text": chatTextField.text! as AnyObject,
            "timesTamp": NSNumber(value: Date().timeIntervalSince1970)
        ]
        
        ref.updateChildValues(data) { (err, ref) in
            guard err == nil else {
                print(err as Any)
                return
            }
            
            self.chatTextField.text = nil
            if let groupId = self.groupKey {
                DataService.instance.groupRef.child(groupId).child("messages").updateChildValues([ref.key: 1])
            }
            
        }
        
    }
    
    
    
    
}
