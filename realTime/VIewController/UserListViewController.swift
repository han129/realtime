//
//  UserListViewController.swift
//  realTime
//
//  Created by 이요한 on 06/08/2019.
//  Copyright © 2019 Yo. All rights reserved.
//

import Foundation
import UIKit

class UserListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tableview: UITableView!
    
    var chatGroupVC: ChatGroupViewController?
    var userList: [User] = []
    
    func fetchUserList() {
        DataService.instance.userRef.observeSingleEvent(of: .value, with: { (snapshot) in
            if let data = snapshot.value as? Dictionary<String, AnyObject>, let uid = DataService.instance.currentUserUid {
                for (key, data) in data {
                    if uid != key {
                        if let userData = data as? Dictionary<String, AnyObject> {
                            let userName = userData["name"] as! String
                            let email = userData["email"] as! String
                            let user = User(uid: uid, email: email, userName: userName)
                            self.userList.append(user)
                            
                            DispatchQueue.main.async(execute: {
                                self.tableview.reloadData()
                                })
                        }
                    }
                }
            }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.dataSource = self
        tableview.delegate = self
        fetchUserList()
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "userCell", for: indexPath)
        cell.textLabel?.text = userList[indexPath.row].userName
        cell.detailTextLabel?.text = userList[indexPath.row].email
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let ref = DataService.instance.groupRef.childByAutoId()
        ref.child("name").setValue(userList[indexPath.row].userName as String)
        dismiss(animated: true) {
            if let chatGroupVC = self.chatGroupVC {
                chatGroupVC.performSegue(withIdentifier: "chatting", sender: ref.key)
            }
        }
        return
    }
    
    @IBAction func cancerButtonTap(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    

    
}
