//
//  DataService.swift
//  realTime
//
//  Created by 이요한 on 05/08/2019.
//  Copyright © 2019 Yo. All rights reserved.
//

import Foundation
import Firebase

fileprivate let baseRef = Database.database().reference()

class DataService {
    static let instance = DataService()
    
    let userRef = baseRef.child("user")
    
    let groupRef = baseRef.child("group")
    
    let messageRef = baseRef.child("message")
    
    var currentUserUid: String? {
        get {
            guard let uid = Auth.auth().currentUser?.uid else {
                return nil
            }
            return uid
        }
    }
    
    func createUserInfoFromAuth(uid: String, userData: Dictionary<String, String>) {
        userRef.child(uid).updateChildValues(userData)
    }
    
    func signIn(email withEmail: String, password: String, completion: @escaping () -> Void) {
        Auth.auth().signIn(withEmail: withEmail, password: password, completion: { (user, error) in
            guard error == nil else {
                print("Error Occurred during Sign In")
                return
            }
            completion()
        })
    }
    
    
}
